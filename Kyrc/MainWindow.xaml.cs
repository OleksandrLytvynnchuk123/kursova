﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CreateUser;
using Microsoft.Win32;

namespace Kyrc
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            MainWindow1 f = new MainWindow1();
            f.ShowDialog();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {

            StreamReader str = Additional.GetStreamReader();
            bool needToFind = false;
            do
            {
                string st = str.ReadLine();
                if (st.StartsWith(SearchBox.Text))
                {
                    needToFind = true;
                    InfoBox.Clear();
                    InfoBox.Text = st;
                }
            } while (!str.EndOfStream);
            if (needToFind == false)
            {
                MessageBox.Show($"Користувача {SearchBox.Text} не існує.");
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            StreamReader str = Additional.GetStreamReader();
            List<string> stMass = new List<string>();
            bool needToDelete = false;
            while (!str.EndOfStream)
            {
                string st = str.ReadLine();
                if (!st.StartsWith(DeleteBox.Text))
                {
                    stMass.Add(st);
                }
                else
                {
                    needToDelete = true;
                }
            }
            str.Close();
            if (needToDelete == false)
            {
                MessageBox.Show($"Користувача {DeleteBox.Text} не існує.");
            }
            if (needToDelete == true)
            {
                System.IO.File.Delete(@"C:\Users.txt");
                foreach (string s in stMass)
                {
                    System.IO.File.AppendAllText(@"C:\Users.txt", s + "\r\n", Encoding.Unicode);
                }
                stMass = null;
                MessageBox.Show($"Данні користувача {DeleteBox.Text} видалено.");
            }

        }

        private void Correct_Click(object sender, RoutedEventArgs e)
        {

            StreamReader str = Additional.GetStreamReader();
            List<string> stMass = new List<string>();
            bool needToRewrite = false;
            while (!str.EndOfStream)
            {
                string st = str.ReadLine();
                
                if (st.StartsWith(CorrectBox.Text))
                {
                    st = st.Replace(st, $"{InfoCorrectBox.Text}");
                    needToRewrite = true;
                }
                stMass.Add(st);
            }
            if(needToRewrite == false)
            {
                MessageBox.Show($"Користувача {CorrectBox.Text} не існує.");
            }
            str.Close();
            if (needToRewrite == true)
            {
                System.IO.File.Delete(@"C:\Users.txt");
                foreach(string s in stMass)
                {
                    System.IO.File.AppendAllText(@"C:\Users.txt", s + "\r\n", Encoding.Unicode);
                    System.IO.File.AppendAllText(@"C:\History.txt", s + "\r\n", Encoding.Unicode);
                }
                stMass = null;
                MessageBox.Show($"Данні користувача {CorrectBox.Text} змінено.");
            }

        }

        private void AddBook_Click(object sender, RoutedEventArgs e)
        {
            StreamReader str = Additional.GetStreamReader();
            List<string> stMass = new List<string>();
            bool needToAdd = false;
            while (!str.EndOfStream)
            {
                string st = str.ReadLine();

                if (st.StartsWith(AddBookBox.Text))
                {
                    st = st + $"{InfoAddBookBox.Text}";
                    needToAdd = true;
                }
                stMass.Add(st);
            }
            if (needToAdd == false)
            {
                MessageBox.Show($"Користувача {AddBookBox.Text} не існує.");
            }
            str.Close();
            if (needToAdd == true)
            {
                System.IO.File.Delete(@"C:\Users.txt");
                foreach (string s in stMass)
                {
                    System.IO.File.AppendAllText(@"C:\Users.txt", s + "\r\n", Encoding.Unicode);
                }
                stMass = null;
                MessageBox.Show($"Данні користувача {AddBookBox.Text} змінено.");
            }
        }

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(@"C:Users.txt");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Process.Start(@"C:History.txt");
        }
    }
}
