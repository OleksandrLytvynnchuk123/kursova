﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreateUser
{
    public partial class MainWindow1 : Window
    {
        public MainWindow1()
        {
            InitializeComponent();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            string line ="\r\n" + NameBox.Text + " |   " + "Дата народження користувача: " + BirthDateBox.Text + " |   " + "Дата реєстрації користувача: " + StartDateBox.Text + " |   " + "Номер телефону: " + PhoneNumberBox.Text;
            System.IO.File.AppendAllText(@"C:\Users.txt", line, Encoding.Unicode);
            System.IO.File.AppendAllText(@"C:\History.txt", line, Encoding.Unicode);
            MessageBox.Show($"Створено користувача {NameBox.Text}.");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string line = " |   " + "Взята книжка: " + BookBox.Text + " |   " + "Дата отримання книжки: " + TakeBox.Text + " |   " + "Кінцева дата повернення книжки: " + ReturnBox.Text;
            System.IO.File.AppendAllText(@"C:\Users.txt", line, Encoding.Unicode);
            System.IO.File.AppendAllText(@"C:\History.txt", line, Encoding.Unicode);
            MessageBox.Show($"Взято книжку {BookBox.Text}");
        }

        private void ClearBox_Click(object sender, RoutedEventArgs e)
        {
            NameBox.Clear();
            BirthDateBox.Clear();
            StartDateBox.Clear();
            PhoneNumberBox.Clear();
            BookBox.Clear();
            TakeBox.Clear();
            ReturnBox.Clear();
        }
    }
}
